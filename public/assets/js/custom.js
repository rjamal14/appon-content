function showLoading(el = 'body') {
    $(el).LoadingOverlay("show", {
        image: imageloading,
        maxSize: 150,
        minSize: 150
    });
}
  
function hideLoading(el = 'body') {
    $(el).LoadingOverlay("hide", true);
}
