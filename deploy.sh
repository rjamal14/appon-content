cd appon-content
git checkout development
git pull origin development
docker-compose down
docker-compose rm -f
docker-compose pull
docker-compose up --build --force-recreate -d
php artisan cache:clear
echo 'Deploy app_appon_content finished.'
