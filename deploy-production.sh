cd /home/deploy/container/appon-content

git checkout production
git pull origin production

docker-compose -f docker-compose-production.yml build --no-cache
docker-compose -f docker-compose-production.yml push
docker stack deploy -c docker-compose-production.yml appon-content --prune

echo 'Deploy app_transaction finished.'
