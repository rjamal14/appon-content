<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'program-promo'], function () {
    Route::get('/', 'ProgramPromoController@getPage')->name('ppromo_page');
    Route::post('/', 'ProgramPromoController@postPpromo')->name('post_ppromo');
});
