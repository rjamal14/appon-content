@extends('layouts.master')

@section('title')
    Program & Promo Participants
@endsection
@section('css')
    <style>
        .text-info-wp{
            background-color: #ffe9e991;
            display: flex;
            flex-direction: column;
            border-radius: 15px; 
            margin-top: 1rem; 
            overflow-wrap: break-word;
        }
        .text-isfollow{    
            background-color: #e0e0e0;
            font-weight: 700;
            padding: 12px;
            border-radius: 15px 15px 0px 0px;
            color: #333;
        }
        .text-desk{ 
            padding: 10px;
            font-size: 15px;
        }   
        .text-desk p{   
            white-space: pre-line;              
            font-size: 13px;     
            margin-top: 10px;  
        }
        .text-desk div{
            font-weight: 500;
        }  
    </style>
@endsection
@section('content')
<form role="form" id="form_ppromo" method="post" enctype="multipart/form-data" files="true">
    <div class="row justify-content-center">
        <div class="text-center">
            @if (isset($promo))                
            <input type="hidden" name="promo" value="{{$promo}}">
            <input type="hidden" name="usr" value="{{$usr}}}}">
            @endif
            <img class="img-fluid mb-4" src="{!! $data->image_detail_url !!}" alt="Responsive image">
        </div>
        <div class="text-center">
            <h6 class="fw-bold fs-4 display-3 lh-sm">{!!$data->title !!}</h6>
            @if (!isset($isfollow))
                <p class="my-4 pe-xl-5" style="white-space: pre-line"> {!! $data->description !!}</p>  
            @endif          
        </div>
        <div class="text-center">
            @if (!isset($isfollow))
                {!! $button !!}
            @else
                <div class="text-info-wp">
                    <div class="text-isfollow">Selamat Anda Berhasil Mengikuti Promo Ini</div>                 
                    <div class="text-desk"> 
                        <div class="text">ⓘ Informasi</div>  
                        <p>{!! $data->description_after !!}</p> 
                    </div>
                </div>
            @endif
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $("#form_ppromo").submit(function(e) {
        e.preventDefault();
        showLoading();        
        axios({
            method: "POST",
            url: "{{route('post_ppromo')}}",
            data: $("#form_ppromo").serialize()
        }).then(function (response_data) {
            hideLoading();    
            if(response_data.data.status=="success" || response_data.data.success){
                location.reload();
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops... ada masalah dengan server kami',
                    text: 'mohon menunggu dan coba kembali lain waktu yaa',
                })
            }         
        })
        .catch(function (error) {
            hideLoading();
            Swal.fire({
                icon: 'error',
                title: 'Oops... gagal mengikuti promo',
                text: 'mohon coba kembali yaa',
            })
        });
    });
</script>
@endsection 