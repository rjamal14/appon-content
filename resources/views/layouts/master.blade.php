<!DOCTYPE html>
<html lang="en-US" dir="ltr">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400&amp;display=swap" rel="stylesheet">

    <title>{{ env('APP_NAME') }} | @yield('title')</title>

    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <link href="{{ asset('assets/css/theme.css') }}" rel="stylesheet">
    @yield('css')
  </head>


  <body>
    <main class="main" id="top">
        <section class="py-3">
          <div class="container">
            @yield('content')
          </div>
        </section>
      </main>
    <script>
        var imageloading = "{{asset('assets/img/loading.gif')}}";
    </script>
    <script src="{{ asset('assets/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/axios.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}?{{time()}}"></script>
    <script src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('assets/js/sweetalert.js') }}"></script>
    <script src="{{ asset('assets/vendors/@popperjs/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/is/is.min.js') }}"></script>
    <script src="{{ asset('assets/js/theme.js') }}"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>    
    @yield('js')
  </body>
</html>