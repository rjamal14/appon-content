@extends('layouts.master')

@section('title')
Welcome
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="text-center">
            <img class="img-fluid mb-4" src="/assets/img/logowebhitam@2x.png" alt="Responsive image">
        </div>
        <div class="text-center">
            <h6 class="fw-bold fs-4 display-3 lh-sm">Aplikasi pengembangan bisnis online</h6>
            <p class="my-4 pe-xl-5" style="white-space: pre-line"> AppOn memudahkan kamu kembangin bisnis semudah belanja online</p>            
        </div>
        <div class="text-center">
            @if (isset($error))
                <div class="badge bg-light rounded-pill text-dark justify-content-center align-items-center vertical-center spanCstm">
                    <div class="d-flex flex-column">
                        <div class="text p-3">{{$error}}</div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection