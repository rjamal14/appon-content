<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session, Crypt, Carbon\Carbon;

class ProgramPromoController extends Controller
{
    public function getPage(Request $request)
    {        
        try {
            $token = $request->bearerToken();
            //** cek promo */
            
            if (isset($request->promo_id)&&isset($request->user_id)) {
                $promo = Http::withToken($token)->get(env('PRODUCT_URI').'/program_promos/'.$request->promo_id);
                $resp_promo = $promo->json(); 
                if (isset($resp_promo['success']) && $resp_promo['success']) {       
                    //** validasi participant promo */
                    $participant = Http::withToken($token)->post(env('TRANSACTION_URI').'/program_promos_participant/check?user_id='.$request->user_id.'&promo_id='.$request->promo_id);
                    $resp_participant = $participant->json();
                    $data['data'] = (object)$resp_promo['result'];   
                    
                    if (isset($resp_participant['status'])) {
                        if ($resp_participant['status']=='success') {
                            Session::put('access_token', $token);                
                            $data['promo'] = Crypt::encryptString($request->promo_id);                  
                            $data['usr'] = Crypt::encryptString($request->user_id);           
                            $data['button'] = '<button type="submit" class="btn btn-lg btn-primary" > Ikuti Promo ini </button>'; 
                            
                            return view('pages.program_promo.index',$data);   
                        } else {
                            $data['isfollow'] = true;      
                            return view('pages.program_promo.index',$data);
                        }
                    } else {    
                        $data['error'] = 'Anda Tidak Memiliki Otoritas';  
                        return view('welcome', $data);
                    }
                } else {         
                    $data['error'] = 'Promo Tidak ditemukan!';  
                    return view('welcome', $data);
                }            
            } else {       
                $data['error'] = '404 Not Found';        
                return view('welcome', $data);
            }  
        } catch (ClientException $e) {
            return $this->response_error($e);
        }
    }
    
    public function postPpromo(Request $request)
    {        
        try {         
            $token = Session::get('access_token');
            $data = [
                'user_id' => Crypt::decryptString(request('usr')),
                'promo_id' => Crypt::decryptString(request('promo')),
                'submit_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'status' => 'pending'
            ];
    
            $stores = Http::withToken($token)->post(env('TRANSACTION_URI').'/program_promos_participant/', $data); 
            return $stores->json();
        } catch (ClientException $e) {
            return $this->response_error($e);
        }
    }
}